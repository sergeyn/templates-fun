#include <iostream>
#include <map>

#include "utils.h"
#include "gtest/gtest.h"

using namespace std;

typedef map<int, double> map_t;
typedef map_t::value_type pair_t;


//function predicate:
bool isLt(const double& d) { return d<30.0;}

//inner is by-ref!
struct inner : std::unary_function<double&, int> { int operator()(double &v) const { v-=1; return v;}};
struct outer : std::unary_function<int, char>{ char operator()(int v) const { v-=1; return v;}};

TEST(TemplateFun, composeLiteWorksAndSmall) {
    double v = 42;
    char res = compose_lite<outer,inner>()(v);
    EXPECT_EQ(40, res);
    EXPECT_EQ(41u, v); //side affected
    EXPECT_EQ(1u,sizeof(compose_lite<outer,inner>)); //no state or stored ptrs
}

TEST(TemplateFun, getSecondReturnsSecond) {
    map_t m1;
    m1[0] = 42.0;

    map_t::const_iterator citm1 = m1.begin();
    map_t::iterator itm1 = m1.begin();

    get_second<pair_t> gsecond;
    get_second<pair_t const> gsecond_c;

    EXPECT_EQ(42,gsecond(*itm1));
    EXPECT_EQ(42,gsecond_c(*citm1));
}

TEST(TemplateFun, predicatesAppliedToValuesWithComposedPredicate) {
    map_t m1;
    m1[0] = 42.0;

    // std::ptr_fun(&isLt) -- wraps the function in a functor with result_type of isLt
    //make_compose creates a composition functor with correct argument_type

    //if predicate_on_map were to accept non-const predicates, we could not pass here an rvalue
    cref_predicate_on_map(m1,
                          make_compose(std::ptr_fun(&isLt), get_second<pair_t>())) ;

    by_val_predicate_on_map(m1,
                            make_compose(std::ptr_fun(&isLt), get_second<pair_t>())) ;

    //wouldn't you just love 'auto' ?
    compose<pointer_to_unary_function<const double&, bool>, get_second<pair_t> > c =
            make_compose(std::ptr_fun(&isLt), get_second<pair_t>());

    EXPECT_FALSE(ref_predicate_on_map(m1,c)); //42<30
    EXPECT_EQ(16u,sizeof(c)); //16 bytes -- two pointers to functions...
}

TEST(TemplateFun, useFilter) {
    map_t m1;
    m1[0] = 42.0;
    m1[1] = 20.0;

    //if only ctor could be used to detect template arguments
    filter_iterator<map_t::iterator, compose<pointer_to_unary_function<const double&, bool>, get_second<pair_t> > >
            fit(m1.begin(),m1.end(),make_compose(std::ptr_fun(&isLt), get_second<pair_t>()));

    filter_iterator<map_t::const_iterator, compose<pointer_to_unary_function<const double&, bool>, get_second<pair_t const> > >
            cfit(m1.begin(),m1.end(),make_compose(std::ptr_fun(&isLt), get_second<pair_t const>()));

    EXPECT_EQ(20.0, (*fit).second);
    EXPECT_EQ(20.0, (*cfit).second);
}

TEST(TemplateFun, useSecondIterator) {
    map_t m1;
    m1[0] = 42.0;
    m1[1] = 20.0;
    //will not work with const_iterator without iterator traits...
    second_iterator<map_t::iterator> it(m1.begin());
    second_iterator<map_t::iterator> end(m1.end());
    map_t::iterator where = m1.begin();
    for(; it != end; ++it, ++where)
        EXPECT_EQ(*it, where->second);

}


int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
