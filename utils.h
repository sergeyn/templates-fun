#ifndef REMOVE_IF_UTILS_H_H
#define REMOVE_IF_UTILS_H_H

#include <map>


template<typename P>
struct second_type_selector { typedef typename P::second_type & ref; };
template<typename P>
struct second_type_selector<const P> { typedef const typename P::second_type & ref; };

//note: P::second_type of a const std::pair<const K,V> is non-const V
//similarly: map<K,V>::const_iterator::value_type (but not map<K,V>::const_iterator::reference)
//hence we use the type_selector to propagate constness to the second of the pair
template<typename P>
struct get_second : public std::unary_function<P&, typename second_type_selector<P>::ref>
{
    using typename std::unary_function<P&, typename second_type_selector<P>::ref>::argument_type;
    using typename std::unary_function<P&, typename second_type_selector<P>::ref>::result_type;

    result_type &operator()(argument_type &arg) const { return arg.second; }
};
//----------------------------------------------------------------------------------------------------
//  F2::arg --> F1::result by F1(F2(arg))
template<typename F1, typename F2, typename R=typename F1::result_type, typename Arg=typename F2::argument_type>
struct compose {
    F1 f1;
    F2 f2;

    typedef R result_type;
    typedef Arg argument_type;

    compose(const F1 &f_1, const F2 &f_2) : f1(f_1), f2(f_2) {}
    result_type operator()(argument_type arg) const { return f1(f2(arg));}
};

template<typename F1, typename F2>
compose<F1,F2> make_compose(const F1 &f_1, const F2 &f_2) { return compose<F1,F2>(f_1,f_2);}


template<typename F1, typename F2, typename R=typename F1::result_type, typename Arg=typename F2::argument_type>
struct compose_lite {
    typedef R result_type;
    typedef Arg argument_type;
    R operator()(Arg arg) const { return F1()(F2()(arg)); }
};
//----------------------------------------------------------------------------------------------------

//note: the predicate is const ref, to allow rvalues
template<typename M, typename P>
bool cref_predicate_on_map(M &map, const P &predicate)
{
    return predicate(*map.begin());
}

//note: the predicate is passed by value
template<typename M, typename P>
bool by_val_predicate_on_map(M &map, P predicate)
{
    return predicate(*map.begin());
}

//note: the predicate is passed by non-const ref
template<typename M, typename P>
bool ref_predicate_on_map(M &map, P& predicate)
{
    return predicate(*map.begin());
}

//----------------------------------------------------------------------------------------------------

template<typename It>
struct iterator_range {
    iterator_range(const It &_first, const It &_last) :
            first(_first), last(_last){}

    typedef typename It::value_type value_type;
    It begin() const { return first; }
    It end() const { return last; }
    It first;
    It last;
};
template<typename It>
iterator_range<It> make_iterator_range(const It &f, const It &l) {
    return iterator_range<It>(f,l);
}

template<typename It, typename P>
struct filter_iterator {
    filter_iterator(const It &_first, const It &_last, P pre) :
            current(_first), last(_last), predicate(pre){
        for(; current != last && !predicate(*current); ++current )
        {}
    }

    typedef typename It::value_type value_type;
    typedef typename It::reference reference;
    reference operator*() const { return *current; }
    filter_iterator& operator++() {
        ++current;
        for(; current != last && !predicate(*current); ++current )
        {}
        return *this;
    };
    bool operator!=(const filter_iterator &rhs) const {
        return current != rhs.current || last != rhs.last;
    }

    It current;
    It last;
    P predicate;
};


template<typename It>
struct second_iterator {
    second_iterator(const It &it) :
            current(it){
    }

    typedef typename It::value_type::second_type value_type;
    //we really want a reference here but without decltype can't remove ref, leaving const
    value_type& operator*() const { return current->second; }
    second_iterator& operator++() {
        ++current;
        return *this;
    };
    bool operator!=(const second_iterator &rhs) const {
        return current != rhs.current;
    }

    It current;
};



#endif //REMOVE_IF_UTILS_H_H
